<?php
/*
Template Name: page
*/
?>
<?php get_header(); ?>
<?php get_carousel();?>
	<div class="container-fluid">
		<div class="minibarra"></div>
		<div class="row-fluid no-space columnaizda" id="<?php if(is_page("media")) {?>media<?php } ?>">
			<div class="span8" id="home_content">
			
			<?php 
				
				if( get_field('prensa') )
{
	while( has_sub_field("prensa") )
	{
		if( get_row_layout() == "lovideo" ) // layout: Paragraph
		{

		} else {
			$url = get_sub_field('url');
			$urlimagen = get_sub_field('urlimagen');
			$titular = get_sub_field('titular');

echo '<p><a target="_blank" href="'.$url.'"><img class="alignnone size-thumbnail wp-image-338 imamedia" alt="Carmen Jordá en los medios" src="'. $urlimagen .'" width="150" height="150" style="min-height:128px;" /><span class="btn btn-info">'.$titular.'</span></a></p>';

		}
	}
}
				
			?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content();?>
				<?php endwhile; 
					
if( get_field('videos') )
{
	while( has_sub_field("videos") )
	{
		if( get_row_layout() == "lovideo" ) // layout: Paragraph
		{

		} else {
			$titulo = get_sub_field('titulo');
			$url = get_sub_field('url');

echo '<p><div class="gvideos"><div class="iconopalm"></div><h3>'.$titulo.'</h3><iframe src="'. $url .'" frameborder="0" allowfullscreen></iframe></div></p>';

		}
	}
}

?>
				
				
			</div>
			<?php 
    			 get_barraderecha();
			 
			 ?>
		</div>

<?php get_footer(); ?>
