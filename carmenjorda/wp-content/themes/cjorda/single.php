<?php get_header(); ?>
    <div class="esnoticia">
    	<?php get_carousel();?>
    </div>
	<div class="container-fluid">
		<div class="minibarra"></div>
		<div class="row-fluid no-space columnaizda">
			<div class="span8" id="home_content">
				
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="noticia">
						<div class="minibarra"></div>
						<img src="<?php the_field("imagen");?>">
						<div class="fecha_noticia">
						<p style="text-transform: uppercase">							
    						<?php $idioma = get_bloginfo('language'); if ($idioma == 'es-ES') { ?>
    						  <?php echo get_the_date('d \d\e ');mes(get_the_date('m')); echo get_the_date(' \d\e Y'); ?>
    						  <?php } else { ?>
							<?php echo get_the_date('m');echo get_the_date('\, d');echo get_the_date('\, Y'); ?>
							<?php } ?>
						</p>

						</div>
						<div class="titulo_noticia"><p><?php the_title();?></p></div>
						<div class="contenido"><?php the_content();?></div>
						<div class="container-fluid container_botones">
							<div class="row-fluid">
								<?php compartir();?>
							</div>
						</div>
					</div>
					<div class="comentarios patata">
						<?php comments_template( '', true ); ?>

					</div>
				<?php endwhile; ?>
			</div>
			<?php get_barraderecha();?>
		</div>

<?php get_footer(); ?>