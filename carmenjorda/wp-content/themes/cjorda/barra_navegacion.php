<div class="barra">
	<div class="container" id="menu_nav">
		<div class="menuiphone visible-phone">
			<div class="btn-group">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
					Menú
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php bloginfo("url");?>/index.php">
							<div class="btn_barra <?php if(is_home())echo 'activeb';?>">
								<p>
									<?php _e("HOME", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/calendario">
							<div class="btn_barra <?php if(is_page("calendario"))echo 'activeb';?>">
								<p>
									<?php _e("CALENDAR", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/galeria">
							<div class="btn_barra <?php if(is_page("galeria"))echo 'activeb';?>">
								<p>
									<?php _e("GALLERY", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/about">
							<div class="btn_barra <?php if(is_page("about-me"))echo 'activeb';?>">
								<p>
									<?php _e("ABOUT ME", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/media">
							<div class="btn_barra <?php if(is_page("media"))echo 'activeb';?>">
								<p>
									<?php _e("MEDIA", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/partners">
							<div class="btn_barra <?php if(is_page("partners"))echo 'activeb';?>">
								<p>
									<?php _e("PARTNERS", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/contacto">
							<div class="btn_barra <?php if(is_page("contacto"))echo 'activeb';?>">
								<p>
									<?php _e("CONTACT", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="menudesktop hidden-phone">
			<a href="<?php bloginfo("url");?>/index.php">
				<div class="btn_barra <?php if(is_home())echo 'activeb';?>">
					<p>
						<?php _e("HOME", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/calendario">
				<div class="btn_barra <?php if(is_page("calendario"))echo 'activeb';?>">
					<p>
						<?php _e("CALENDAR", "cjorda"); ?>
					</p>
				</div>
			</a>
			<div class="dropdown">
				<a class="dropdown-toggle" href="<?php bloginfo("url");?>/galeria" >
					<div class="btn_barra <?php if(is_page("galeria"))echo 'activeb';?>">
						<p>
							<?php _e("GALLERY", "cjorda"); ?>
						</p>
					</div>
				</a>
				<ul class="galer dropdown-menu" role="menu">
					<li role="menuitem"><a href="<?php bloginfo("url");?>/galeria">Fotos</a></li>
					<li role="menuitem"><a href="<?php bloginfo("url");?>/galeria/videos">Vídeos</a></li>
				</ul>
			</div>
			
			<a href="<?php bloginfo("url");?>/about-me">
				<div class="btn_barra <?php if(is_page("about-me"))echo 'activeb';?>">
					<p>
						<?php _e("ABOUT ME", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/media">
				<div class="btn_barra <?php if(is_page("media"))echo 'activeb';?>">
					<p>
						<?php _e("MEDIA", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/partners">
				<div class="btn_barra <?php if(is_page("partners"))echo 'activeb';?>">
					<p>
						<?php _e("PARTNERS", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/contacto">
				<div class="btn_barra <?php if(is_page("contacto"))echo 'activeb';?>">
					<p>
						<?php _e("CONTACT", "cjorda"); ?>
					</p>
				</div>
			</a>
			<div class="botonesredes">
				<a target="_blank" href="https://www.facebook.com/carmenjordaofficial" class="redes">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/facebook3.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://twitter.com/carmenjorda" class="redes">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/twitter3.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://instagram.com/carmenjorda#" class="redes youtube">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/instaboton.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://www.youtube.com/carmenjordaofficial" class="redes youtube">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/youtube3.png" alt="">
					</div>
				</a>
				<a href="<?php $idioma = get_bloginfo('language'); if ($idioma == 'es-ES') { ?><?php bloginfo("url");?>/en" class="ingles<?php } else { ?>../../" class="español<?php } ?> bandera">
				    <img style="margin-left: -16px; margin-top: -116px; position: absolute; z-index: -3 ! important;" src="<?php bloginfo("template_url");?>/img/banderas<?php if ($idioma == 'es-ES') {  } else {?>2<?php }?>.png" alt="">
				</a>
			</div>
		</div>
	</div>
</div>

<!-- BARRA OCULTA PARA SCROLL -->

<div class="barra menuscroll">
	<div class="container" id="menu_nav2">
		<div class="menuiphone visible-phone">
			<div class="btn-group">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
					Menú
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
					<li>
						<a href="<?php bloginfo("url");?>/index.php">
							<div class="btn_barra <?php if(is_home())echo 'activeb';?>">
								<p>
									<?php _e("HOME", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/calendario">
							<div class="btn_barra <?php if(is_page("calendario"))echo 'activeb';?>">
								<p>
									<?php _e("CALENDAR", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/galeria">
							<div class="btn_barra <?php if(is_page("galeria"))echo 'activeb';?>">
								<p>
									<?php _e("GALLERY", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/about">
							<div class="btn_barra <?php if(is_page("about-me"))echo 'activeb';?>">
								<p>
									<?php _e("ABOUT ME", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/media">
							<div class="btn_barra <?php if(is_page("media"))echo 'activeb';?>">
								<p>
									<?php _e("MEDIA", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/partners">
							<div class="btn_barra <?php if(is_page("partners"))echo 'activeb';?>">
								<p>
									<?php _e("PARTNERS", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php bloginfo("url");?>/contacto">
							<div class="btn_barra <?php if(is_page("contacto"))echo 'activeb';?>">
								<p>
									<?php _e("CONTACT", "cjorda"); ?>
								</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="menudesktop hidden-phone">
			<a href="<?php bloginfo("url");?>/index.php">
				<div class="btn_barra <?php if(is_home())echo 'activeb';?>">
					<p>
						<?php _e("HOME", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/calendario">
				<div class="btn_barra <?php if(is_page("calendario"))echo 'activeb';?>">
					<p>
						<?php _e("CALENDAR", "cjorda"); ?>
					</p>
				</div>
			</a>
			<div class="dropdown">
				<a class="dropdown-toggle" href="<?php bloginfo("url");?>/galeria">
					<div class="btn_barra <?php if(is_page("galeria"))echo 'activeb';?>">
						<p>
							<?php _e("GALLERY", "cjorda"); ?>
						</p>
					</div>
				</a>
				<ul class="galer dropdown-menu" role="menu">
					<li role="menuitem"><a href="<?php bloginfo("url");?>/galeria">Fotos</a></li>
					<li role="menuitem"><a href="<?php bloginfo("url");?>/galeria/videos">Vídeos</a></li>
				</ul>
			</div>
			<a href="<?php bloginfo("url");?>/about-me">
				<div class="btn_barra <?php if(is_page("about-me"))echo 'activeb';?>">
					<p>
						<?php _e("ABOUT ME", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/media">
				<div class="btn_barra <?php if(is_page("media"))echo 'activeb';?>">
					<p>
						<?php _e("MEDIA", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/partners">
				<div class="btn_barra <?php if(is_page("partners"))echo 'activeb';?>">
					<p>
						<?php _e("PARTNERS", "cjorda"); ?>
					</p>
				</div>
			</a>
			<a href="<?php bloginfo("url");?>/contacto">
				<div class="btn_barra <?php if(is_page("contacto"))echo 'activeb';?>">
					<p>
						<?php _e("CONTACT", "cjorda"); ?>
					</p>
				</div>
			</a>
			<div class="botonesredes">
				<a target="_blank" href="https://www.facebook.com/carmenjordaofficial" class="redes">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/facebook3.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://twitter.com/carmenjorda" class="redes">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/twitter3.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://instagram.com/carmenjorda#" class="redes youtube">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/instaboton.png" alt="">
					</div>
				</a>
				<a target="_blank" href="http://www.youtube.com/carmenjordaofficial" class="redes youtube">
					<div class="btn_barra">
						<img src="<?php bloginfo("template_url");?>/img/youtube3.png" alt="">
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
