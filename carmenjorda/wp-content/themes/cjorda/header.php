<?php
/**
 * @package Bueninvento
 * @subpackage CJorda
 * @since Carmen Jorda
 */
?> 

<!DOCTYPE html>
	<!--[if lt IE 7]>	<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>	<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]>	<html class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!-->	<html class="no-js" <?php if ($idioma == 'es-ES') { ?>lang="es"<?php } else { ?>lang="en"<?php } ?>> <!--<![endif]-->

	<!-- 0. HEAD -->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
		<title><?php bloginfo('name'); ?> | <?php is_404() || is_home() ? bloginfo('description') : wp_title(''); ?></title>
		<meta name="description" content="Carmen Jordá | GP3 driver | GP3 Bamboo Engineering | Sitio web oficial" />
		<link rel="canonical" href="http://www.carmenjorda.com/" />
		<meta name="keywords" content="Carmen Jordá, GP3, GP3 series, Jorda, Alcoy, Valencia, racing, Bamboo, piloto carreras, mujer, women drivers, mujer piloto, velocidad, carreras" />
		<meta name="author" content="bueninvento.es" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" />
		
		<?php estilos();?>
		<?php javascripts();?>
		<?php wp_head(); ?>
	</head>
	<?php 
		if(is_home())
			$page="home";
		else if(is_page("circuitos"))
			$page="circuitos";
		else if(is_page("galeria"))
			$page="galeria";
		else if(is_page("lagal"))
			$page="galeria";
		else if(is_page("videos"))
			$page="galeria";
		else if(is_page("about-me"))
			$page="aboutme";
		else if(is_page("partners"))
			$page="partners";
		else if(is_page("calendario"))
			$page="calendario";
		else if(is_page("contacto"))
			$page="contacto";
		else
			$page="otra";
	?>
	<body id="<?php echo $page;?>" <?php if($page=="home"):?>onload="hide_preloader()"<?php endif;?>>
	


		<?php if($page=="home"):?>
		<div id="preloader">
			<div class="datospreloader">
				<p><?php _e("Loading, please wait", "cjorda"); ?></p>
				<img src="<?php bloginfo("template_url");?>/img/mascara.png" alt="" id="mascarapreloader">
				<img src="<?php bloginfo("template_url");?>/img/rpm.gif" id="preloader_image" alt="">
				<img src="<?php bloginfo("template_url");?>/img/logocjorda.png" id="prelogo" alt="">
			</div>
		</div> 
		<?php endif;?>
	<header>
		<div class="container" id="header">
		      <div id="logo">
		          <div id="ipadlogo">
		          	<a href="<?php bloginfo("url");?>/index.php">
			          	<img src="<?php bloginfo("template_url");?>/img/logodegradado.png" alt="" class="hidden-tablet hidden-phone">
			          	<img src="<?php bloginfo("template_url");?>/img/logocjorda.png" alt="" class="hidden-desktop">
		          	</a>
		          </div>
		      </div>
			<?php get_crono();?>
		</div>
		<?php get_barra_nav();?>
	</header>
		<div class="container sconta">