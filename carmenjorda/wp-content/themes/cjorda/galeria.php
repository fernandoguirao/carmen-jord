<?php
/*
Template Name: galeria2
*/
?>
<?php get_header(); ?>
<?php get_carousel();?>
	<div class="container-fluid">
		<div class="minibarra"></div>
		<div class="row-fluid no-space columnaizda" id="<?php if(is_page("media")) {?>media<?php } ?>">
			<div class="span8" id="home_content">
				<!-- EMPIEZA LA GALERÍA -->
				<?php $idioma = get_bloginfo('language'); 
				if ($idioma == 'es-ES') { ?> 
					<div class='alert alert-block alert-error fade in visible-phone' style="margin-top:40px;">La galería fotográfica completa sólo se encuentra disponible en la versión de esta web para pantallas más grandes.</div>
				<?php } else { ?> 
					<div class='alert alert-block alert-error fade in visible-phone'>The full photo gallery is only avaliable on larger screens.</div>
				<?php } ?>

	<div class="unslide año13 visible-phone">
		<div class="tituloslide">
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(37); ?></div>
	</div>
				
				<div class="fotoos hidden-phone">
	
	<div class="unslide año13">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/bamboo.png" />
			<br>
			<h2 class="theyear">
				2013
			</h2>
			<ul>
				<!-- <li><div class="btn btn-primary slb01">Bamboo Engineering GP3</div></li>
				<li><div class="btn btn-info slb02"> MRF Championship</div></li> -->
<?php

if( get_field('tituloslide') )
{
	while( has_sub_field("tituloslide") )
	{
		if( get_row_layout() == "titul" ) // layout: Paragraph
		{

		} else {
			$titulo = get_sub_field('titulo');
			$posicion = get_sub_field('posicion');
			
			if ($posicion < 2) {
				echo '<li><div class="btn btn-primary slb'. $posicion.'">'. $titulo .'</div></li>';
			} else {
				echo '<li><div class="btn btn-info slb'. $posicion.'">'. $titulo .'</div></li>';
			}

		}
	}
}

?>
			</ul>
		</div>
		<!-- <div class="elslide sl01"><?php echo get_new_royalslider(37); ?></div>
		<div class="elslide sl02"><?php echo get_new_royalslider(35); ?></div> -->
<?php

if( get_field('tituloslide') )
{
	while( has_sub_field("tituloslide") )
	{
		if( get_row_layout() == "titul" ) // layout: Paragraph
		{

		} else {
		
			$posicion = get_sub_field('posicion');
			$royalid = get_sub_field('royalid');
			
			echo '<div class="elslide sl'.$posicion.'">'. get_new_royalslider($royalid). '</div>';
		}
	}
}

?>

	</div>

	<div class="unslide año12">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="<?php bloginfo("template_url");?>/img/2012.png" />
			<br>
			<h2 class="theyear">
				2012
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">Ocean Racing GP3</div></li>
				<li><div class="btn btn-info slb05">500M Brasil</div></li>
				<li><div class="btn btn-info slb02">Editorial: Telva</div></li>
				<li><div class="btn btn-info slb03">Kellogs</div></li>
				<li><div class="btn btn-info slb04">Kerastase: Mujer hoy</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(14); ?></div>
		<div class="elslide sl02"><?php echo get_new_royalslider(15); ?></div>
		<div class="elslide sl03"><?php echo get_new_royalslider(16); ?></div>
		<div class="elslide sl04"><?php echo get_new_royalslider(17); ?></div>
		<div class="elslide sl05"><?php echo get_new_royalslider(36); ?></div>
	</div>

	<div class="unslide año11">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/blancpain.png" />
			<br>
			<h2 class="theyear">
				2011
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">Auto GP & Lamorgini Blancpain</div></li>
				<li><div class="btn btn-info slb06">Disney Cars, filming voice</div></li>
				<li><div class="btn btn-info slb04">Reto Velocidad Cheste</div></li>
				<li><div class="btn btn-info slb03">Mia Magazine</div></li>
				<li><div class="btn btn-info slb05">Harpers Bazaar</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(9); ?></div>
		<div class="elslide sl03"><?php echo get_new_royalslider(10); ?></div>
		<div class="elslide sl04"><?php echo get_new_royalslider(13); ?></div>
		<div class="elslide sl05"><?php echo get_new_royalslider(12); ?></div>
		<div class="elslide sl06"><?php echo get_new_royalslider(11); ?></div>
	</div>

	<div class="unslide año10">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/indy.png" />
			<br>
			<h2 class="theyear">
				2010
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">Indy Lights Andersen</div></li>
				<li><div class="btn btn-info slb02">Adidas - Cuatro</div></li>
				<li><div class="btn btn-info slb03">Editorial: Hola</div></li>
				<li><div class="btn btn-info slb04">Editorial: Pronovias</div></li>
				<li><div class="btn btn-info slb05">GQ Awards</div></li>
				<li><div class="btn btn-info slb07">GQ Shooting</div></li>
				<li><div class="btn btn-info slb06">Larios</div></li>
			</ul>
		</div>
		<div class="elslide sl01 lindii">
			<div class="indilights04">
				<?php echo get_new_royalslider(32); ?>
			</div>
			<div class="indilights01">
				<?php echo get_new_royalslider(22); ?>
			</div>
			<div class="indilights02">
				<?php echo get_new_royalslider(30); ?>
			</div>
			<div class="indilights03">
				<?php echo get_new_royalslider(31); ?>
			</div>
			<div class="indilights05">
				<?php echo get_new_royalslider(33); ?>
			</div>
			<div class="indilights06">
				<?php echo get_new_royalslider(34); ?>
			</div>
			<div class="indilights07">
				<?php echo get_new_royalslider(29); ?>
			</div>
			<ul class="lindi">
				<li><div class="btn btn-primary sslb04">Indy Pre Season</div></li>
				<li><div class="btn btn-info sslb01">Alabama GP</div></li>
				<li><div class="btn btn-info sslb02">Edmonton</div></li>
				<li><div class="btn btn-info sslb03">Homestead First Day Oval</div></li>
				<li><div class="btn btn-info sslb05">Long Beach</div></li>
				<li><div class="btn btn-info sslb06">St. Petersburgo</div></li>
				<li><div class="btn btn-info sslb07">Toronto</div></li>
			</ul>
		</div>
		<div class="elslide sl02"><?php echo get_new_royalslider(23); ?></div>
		<div class="elslide sl03"><?php echo get_new_royalslider(25); ?></div>
		<div class="elslide sl04"><?php echo get_new_royalslider(26); ?></div>
		<div class="elslide sl05"><?php echo get_new_royalslider(27); ?></div>
		<div class="elslide sl06"><?php echo get_new_royalslider(28); ?></div>
		<div class="elslide sl07"><?php echo get_new_royalslider(24); ?></div>
	</div>
	
	<div class="unslide año09" style="margin-top:120px!important;">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/lemans.png" />
			<br>
			<h2 class="theyear">
				2009
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">European F3</div></li>
				<li><div class="btn btn-info slb02">Le Mans Series</div></li>
				<li><div class="btn btn-info slb03">Test World Series Light</div></li>
				<li><div class="btn btn-info slb04">Reportaje: Dominical</div></li>
				<li><div class="btn btn-info slb06">Campaign: Chica Tampax</div></li>
				<li><div class="btn btn-info slb05">Editorial: Yo Dona</div></li>
				<li><div class="btn btn-info slb07">Premios Cosmopolitan</div></li>
				<li><div class="btn btn-info slb08">Shooting: Germain de Capuccini</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(5); ?></div>
		<div class="elslide sl02"><?php echo get_new_royalslider(6); ?></div>
		<div class="elslide sl03"><?php echo get_new_royalslider(7); ?></div>
		<div class="elslide sl04"><?php echo get_new_royalslider(18); ?></div>
		<div class="elslide sl05"><?php echo get_new_royalslider(19); ?></div>
		<div class="elslide sl06"><?php echo get_new_royalslider(20); ?></div>
		<div class="elslide sl07"><?php echo get_new_royalslider(21); ?></div>
		<div class="elslide sl08"><?php echo get_new_royalslider(38); ?></div>
	</div>
	
	<div class="unslide año08">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="<?php bloginfo("template_url");?>/img/2008b.png" />
			<br>
			<h2 class="theyear">
				2008
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">European F3</div></li>
				<li><div class="btn btn-info slb02">Editorial: Pepe Jeans - The best Sport</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(39); ?></div>
		<div class="elslide sl02"><?php echo get_new_royalslider(4); ?></div>
	</div>

	<div class="unslide año07">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/meycom.png" />
			<br>
			<h2 class="theyear">
				2007
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">F3 Meycom</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(3); ?></div>
	</div>
	
	<div class="unslide año06">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/bmw.png" />
			<br>
			<h2 class="theyear">
				2006
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">Formula BMW</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(2); ?></div>
	</div>

	<div class="unslide año05">
		<div class="tituloslide">
			<img class="theemblema" alt="" src="http://www.carmenjorda.com/wp-content/uploads/2013/03/karting.png" />
			<br>
			<h2 class="theyear">
				2001<br />2005
			</h2>
			<ul>
				<li><div class="btn btn-primary slb01">Karting</div></li>
			</ul>
		</div>
		<div class="elslide sl01"><?php echo get_new_royalslider(1); ?></div>
	</div>
	
</div>

				<!-- TERMINA LA GALERÍA -->
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content();?>
				<?php endwhile; ?>
			</div>
			<?php 
    			 get_barraderecha();
			 
			 ?>
		</div>
		<script type="text/javascript">

				$(document).ready(function(){
/* 					$('.sl02,.sl03,.sl04,.sl05,.sl06,.sl07').addClass("ocultaar"); */

				})
/* 		FILTRO GALERÍAS */
			$('.slb01').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb05,.slb06,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb05,.slb06,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl02,.sl03,.sl04,.sl05,.sl06,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl01').removeClass("ocultaar");
			})
			$('.slb02').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb01,.slb03,.slb04,.slb05,.slb06,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb01,.slb03,.slb04,.slb05,.slb06,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl04,.sl05,.sl06,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl02').removeClass("ocultaar");
			})
			$('.slb03').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb01,.slb04,.slb05,.slb06,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb01,.slb04,.slb05,.slb06,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl02,.sl04,.sl05,.sl06,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl03').removeClass("ocultaar");
			})
			$('.slb04').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb01,.slb05,.slb06,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb01,.slb05,.slb06,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl02,.sl05,.sl06,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl04').removeClass("ocultaar");
			})
			$('.slb05').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl04,.sl02,.sl06,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl05').removeClass("ocultaar");
			})
			$('.slb06').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb05,.slb07,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb05,.slb07,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl04,.sl02,.sl05,.sl07,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl06').removeClass("ocultaar");
			})
			$('.slb07').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb05,.slb08').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb05,.slb08').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl04,.sl02,.sl06,.sl05,.sl08').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl07').removeClass("ocultaar");
			})
			$('.slb08').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb05,.slb07').removeClass('btn-primary');
				$(this).parent().parent().find('.slb02,.slb03,.slb04,.slb01,.slb06,.slb05,.slb07').addClass('btn-info');
				$(this).parent().parent().parent().parent().find('.sl01,.sl03,.sl04,.sl02,.sl06,.sl05,.sl07').addClass("ocultaar");
				$(this).parent().parent().parent().parent().find('.sl08').removeClass("ocultaar");
			})
			
			
/* 		FILTRO INDI */
			$('.sslb01').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb04,.sslb05,.sslb06,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb04,.sslb05,.sslb06,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights04,.indilights03,.indilights02,.indilights05,.indilights06,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights01').removeClass("ocultaar");
			})
			$('.sslb02').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb04,.sslb03,.sslb01,.sslb05,.sslb06,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb04,.sslb03,.sslb01,.sslb05,.sslb06,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights03,.indilights04,.indilights05,.indilights06,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights02').removeClass("ocultaar");
			})
			$('.sslb03').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb04,.sslb01,.sslb05,.sslb06,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb04,.sslb01,.sslb05,.sslb06,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights04,.indilights02,.indilights05,.indilights06,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights03').removeClass("ocultaar");
			})
			$('.sslb04').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb05,.sslb06,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb05,.sslb06,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights03,.indilights02,.indilights05,.indilights06,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights04').removeClass("ocultaar");
			})
			$('.sslb05').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb06,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb06,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights03,.indilights02,.indilights04,.indilights06,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights05').removeClass("ocultaar");
			})
			$('.sslb06').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb05,.sslb07').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb05,.sslb07').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights03,.indilights02,.indilights04,.indilights05,.indilights07').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights06').removeClass("ocultaar");
			})
			$('.sslb07').click(function(){
				$(this).removeClass('btn-info');
				$(this).addClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb06,.sslb05').removeClass('btn-primary');
				$(this).parent().parent().find('.sslb02,.sslb03,.sslb01,.sslb04,.sslb06,.sslb05').addClass('btn-info');
				$(this).parent().parent().parent().find('.indilights01,.indilights03,.indilights02,.indilights04,.indilights06,.indilights05').addClass("ocultaar");
				$(this).parent().parent().parent().find('.indilights07').removeClass("ocultaar");
			})

		</script>

<?php get_footer(); ?>