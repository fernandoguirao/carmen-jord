<div class="span4 hidden-phone" id="sidebar">
	<?php
	$args = array(
		'tag'      => 'favorita',
		'showposts' => 1
	);
	query_posts( $args );
	while ( have_posts() ) : the_post(); ?>
	<a href="<?php the_permalink();?>">
		<div class="noticia_destacada" style="background-image:url(<?php the_field("slideimagen");?>);background-size:141% auto;">
			<p class="ultimahora">
				<?php _e("STORY OF THE MONTH", "cjorda"); ?>
			</p>
			<br>
			<p style="margin-top: -1px;">
				<?php the_title();?>
			</p>
		</div>
	</a>
	<?php endwhile;?>
	<?php wp_reset_query(); ?>
	<div class="buscador">
		<?php get_buscador(); ?>
	</div>
	<?php
		add_filter( 'wp_feed_cache_transient_lifetime' , 'return_7200' );
		//include_once(ABSPATH . WPINC . '/feed.php');
		//$rss = fetch_feed('https://api.twitter.com/1/statuses/user_timeline.rss?screen_name=CarmenJorda');
		
		//$maxitems = $rss->get_item_quantity(2);
		/*$rss_items = $rss->get_items(0, $maxitems);*/
		remove_filter( 'wp_feed_cache_transient_lifetime' , 'return_7200' );
	?>
	<div class="pand">
		<?php
		foreach ( $rss_items as $item ) : ?>
		<a target="_blank" href="<?php echo $item->get_permalink();?>">
			<div class="entrada_derecha">
				<div class="contenedor_icono">
					<img src="<?php bloginfo("template_url");?>/img/twitter.png" alt="">
				</div>
				<div class="conte">
					<p>
						<?php echo substr($item->get_title(), 12)?>
					</p>
					<p class="lafecha">
						<?php _e("Published on ", "cjorda"); ?><?php $idioma = get_bloginfo('language'); if ($idioma == 'es-ES') { echo $item->get_date('d\.m\.Y'); } else { echo $item->get_date('m\.d\.Y'); } ?>
					</p>
				</div>
			</div>
		</a>		
		<?php endforeach; ?>

<!--
		<span style="width: 100%; display: inline-block; padding-left: 23px; margin-left: -23px; padding-top: 9px; padding-bottom: 9px; margin-top: 9px; margin-bottom: -7px; color: white; overflow: hidden; height: 26px; border-top: 1px dotted rgb(225, 225, 225); background: none repeat scroll 0px 0px rgb(225, 225, 225);">
			<div style="float: left; font-size: 13px; margin-top: 7px; font-family: klavika; font-weight: 300; text-transform: lowercase; color: white; margin-left: 9px;">
				<b style="text-transform: uppercase; margin-right: 5px;">
					Carmen Jordá
				</b>
				 en
			</div>
			<div style="height: 30px; background-image: url(&quot;http://d36xtkk24g8jdx.cloudfront.net/bluebar/2e6f879/images/shared/shared-assets.png&quot;); width: 101px; background-position: 0px -99px; margin-left: 125px;" class="instaloogo">
			</div>
		</span>
-->


		<div class="entrada_derecha instaclass">
			<div class="contenedor_icono">
				<img src="<?php bloginfo("template_url");?>/img/instaicon.png" alt="">
			</div>
			<div class="conte instaimagen">
				<?php echo do_shortcode('[alpine-phototile-for-instagram user="carmenjorda" src="user_recent" imgl="instagram" style="vertical" size="M" num="1" align="center" max="100"]') ?>
				<img src="<?php bloginfo("template_url");?>/img/instathumb.jpg" alt="" class="laimg">
			</div>
		</div>
	</div>
</div>