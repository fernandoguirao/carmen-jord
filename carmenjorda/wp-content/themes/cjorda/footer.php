		</div>

		<?php wp_footer(); ?>
		<footer>
			<div class="footer">
			     <div id="foot-izq">
    				<div class="menu_footer hidden-phone">
    					<a href="<?php bloginfo("url");?>" class="home"><?php _e("HOME", "cjorda"); ?></a>
    					<a href="<?php bloginfo("url");?>/calendario" class="calendario"><?php _e("CALENDAR", "cjorda"); ?></a>
    					<a href="<?php bloginfo("url");?>/galeria" class="galeria"><?php _e("GALLERY", "cjorda"); ?></a>
    					
    					<a href="<?php bloginfo("url");?>/about" class="about-me"><?php _e("ABOUT ME", "cjorda"); ?></a>
    					<a href="<?php bloginfo("url");?>/contacto" class="contacto"><?php _e("CONTACT", "cjorda"); ?></a>
    					<a href="<?php bloginfo("url");?>/partners" class="partners"><?php _e("PARTNERS", "cjorda"); ?></a>
    				</div>
    				<div class="social_footer">
        				<a href="https://www.facebook.com/carmenjordaofficial">
        				    <img src="<?php bloginfo("template_url");?>/img/facebook.png" alt="">
        				</a>
        				<a href="http://twitter.com/carmenjorda">
        				    <img src="<?php bloginfo("template_url");?>/img/twitter.png" alt="">
        				</a>
        				<a href="http://instagram.com/carmenjorda#">
        				    <img src="<?php bloginfo("template_url");?>/img/instagram.png" alt="">
        				</a>
        				<a href="http://www.youtube.com/carmenjordaofficial">
        				    <img src="<?php bloginfo("template_url");?>/img/youtube.png" alt="">
        				</a>
    				</div>
    				<div class="copyright">
    					©Carmen Jorda 2013 | <a href=""><?php _e("Legal terms", "cjorda"); ?></a> | <a href="<?php $idioma = get_bloginfo('language'); if ($idioma == 'es-ES') { ?><?php bloginfo("url");?>/en">English version<?php } else { ?>../../">Versión española<?php } ?></a> |
    					<a href="#popover" id="popoverbtn" data-toggle="popover" data-placement="top" data-content="©Pau Palacios" title="" data-original-title="Fotografías"><?php  if ($idioma == 'es-ES') { ?>Créditos fotografías<?php } else { ?>Photo credits<?php } ?></a>
    				<script type="text/javascript">
	    				$('#popoverbtn').popover();
    				</script>
    				</div>
				</div>
				<div class="patrocinadores_footer visible-desktop" style="">
					<div class="patrocinador_footer">
						<!-- <a href="" target="_blank"> -->
							<img src="<?php bloginfo("template_url");?>/img/carmenfirma.png" alt="">
						<!-- </a> -->
					</div>
				</div>
			</div>
		</footer>
</div>
		<?php get_scripts_footer();?>

	</body>

</html>