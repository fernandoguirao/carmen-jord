<?php
/**
 * @package Bueninvento
 * @subpackage CJorda
 * @since Carmen Jorda
 */
 
 load_theme_textdomain('cjorda');

function estilos(){
    ?>
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fonts/fonts.css">
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap-responsive.min.css">
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/app.css">
    	<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie.css" />
		<![endif]-->
		<!--[if lt IE 9]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie8.css" />
		<![endif]-->
		<!--[if lt IE 8]>
			<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/ie7.css" />
		<![endif]-->
    	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsivo.css">
    <?php
}



function javascripts(){
	?>
		<script src="<?php bloginfo('template_url');?>/js/jquery-1.9.1.min.js"></script>
		<script src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
		

	<?php
}

function get_scripts_footer() {
?>

        <script src="<?php bloginfo('template_url');?>/js/main.js"></script>
        <script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-40674729-1', 'carmenjorda.com');
			ga('send', 'pageview');
		</script>

<?php

}

function get_carousel($index){
	if(is_home() || $index)
		require 'carousel.php';
	else
		require 'banner.php';
}
function get_barra_nav(){
	require 'barra_navegacion.php';
}
function get_noticias_box(){
	require 'noticias_box.php';
}

function get_barraderecha(){
	require 'barra_derecha.php';
}

function get_crono(){
	require 'crono.php';
}

function mandar_email(){
	$invalido=0;
	$cadena="";
	if(isset($_POST["mandado"])){
		if($_POST["acepta"]!="Yes"){
			$cadena.="<div class='alert alert-block alert-error fade in'>Debes aceptar nuestra política de privacidad. | You must agree our privacy police.</div><br>";
			$invalido=1;
		}
		if($_POST["nombre"]==""){
			$cadena.="<div class='alert alert-block alert-error fade in'>Debes introducir tu nombre. | You must introduce your name.</div><br>";
			$invalido=1;
		}
		if($_POST["email"]==""){
			$cadena.="<div class='alert alert-block alert-error fade in'>Debes introducir tu email. | You must introduce your email.</div><br>";
			$invalido=1;
		}
		if($_POST["asunto"]==""){
			$cadena.="<div class='alert alert-block alert-error fade in'>Debes introducir un asunto. | You must introduce a subject.</div><br>";
			$invalido=1;
		}
		if($_POST["contenido"]==""){
			$cadena.="<div class='alert alert-block alert-error fade in'>Debes introducir un mensaje. | You must introduce a message.</div><br>";
			$invalido=1;
		}
		if($invalido==0){
			$cuerpo="";
			$cuerpo.="Nombre: ".$_POST["nombre"]."\n";
			$cuerpo.="Email: ".$_POST["email"]."\n";
			$cuerpo.="Asunto: ".$_POST["asunto"]."\n";
			$cuerpo.="Mensaje: ".$_POST["contenido"]."\n";
			if(mail('fernando@bueninvento.es,info@carmenjorda.com,esther.valle@carmenjorda.com','Mensaje desde el formulario de carmenjorda.com:',$cuerpo))
				$cadena="Mensaje enviado. Gracias.<br>";
			else
				$cadena="Hubo un problema. Intenta enviar de nuevo tu mensaje<br>";
		}
		echo $cadena;
	}
}

function get_buscador() {
?>
	<?php $idioma = get_bloginfo('language');	?>
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
	    <img src="<?php bloginfo("template_url");?>/img/lupa.png" alt="">
	    <input type="text" placeholder="<?php if ($idioma == 'es-ES') { ?>BUSCAR...<?php } else { ?>SEARCH...<?php } ?>" class="inp_buscar" value="" name="s" id="s" />
    </form><?
}

function mes($mes){
$idioma = get_bloginfo('language');	
if ($idioma == 'es-ES') {

	switch($mes){

		case "01":
			echo "enero";
			break;
		case "02":
			echo "febrero";
			break;
		case "03":
			echo "marzo";
			break;
		case "04":
			echo "abril";
			break;
		case "05":
			echo "mayo";
			break;
		case "06":
			echo "junio";
			break;
		case "07":
			echo "julio";
			break;
		case "08":
			echo "agosto";
			break;
		case "09":
			echo "septiembre";
			break;
		case "10":
			echo "octubre";
			break;
		case "11":
			echo "noviembre";
			break;
		case "12":
			echo "diciembre";
			break;
}
}else{
    switch($mes){
		case "01":
			echo "January";
			break;
		case "02":
			echo "Febrary";
			break;
		case "03":
			echo "March";
			break;
		case "04":
			echo "April";
			break;
		case "05":
			echo "May";
			break;
		case "06":
			echo "Juny";
			break;
		case "07":
			echo "July";
			break;
		case "08":
			echo "August";
			break;
		case "09":
			echo "September";
			break;
		case "10":
			echo "October";
			break;
		case "11":
			echo "November";
			break;
		case "12":
			echo "December";
			break;
}

	}
}

function compartir(){
?>				
	<div class="span5 btn_compartir">
		<a href="http://www.facebook.com/sharer.php?
		s=100
		&p[url]=<?php the_permalink();?>
		&p[title]=<?php the_title(); ?>
		&p[summary]=<?php echo get_the_excerpt(); ?>"  
		target="_blank">
		<div class="ico-face">
		</div>
		</a>
		<a href="https://twitter.com/share?url=<?php echo the_permalink();?>&text=<?php echo the_permalink();?> <?php the_title();?>&via=CarmenJorda&lang=es" target="_blank">
			<div class="ico-twit">
			</div>
		</a>
		<span>
			Compartir
		</span>
	</div>


<?php
}

add_action('init', 'remove_header_info');
function remove_header_info() {
	remove_action('wp_head', 'qtrans_header');
}
function return_7200( $seconds )
{
  // change the default feed cache recreation period to 2 hours
  return 120;
}

?>