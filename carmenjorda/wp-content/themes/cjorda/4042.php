<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage CJorda
 * @since Cjorda
 */
 
get_header(); ?>
	<?php get_carousel();?>
	<div class="container-fluid">
		<div class="minibarra"></div>
		<div class="row-fluid no-space columnaizda">
			<div class="span8 busque" id="home_content">
				
					<div class="noticia busque">
						<div class="minibarra"></div>
						
						
						<div class="titulo_noticia"><p></p></div>
						<div class="contenido">
							<?php  if ($idioma == 'es-ES') { ?>
								<div class='alert alert-block alert-error fade in'>La página que buscas no existe. Prueba a escribir de nuevo la URL o realizar una búsqueda de su contenido. Gracias.</div>
							<?php } else { ?>
								<div class='alert alert-block alert-error fade in'>The page you're looking for doesn't exist. Try to write again the URL or make a search. Thanks.</div>
							<?php } ?>
						</div>
						<div class="container-fluid container_botones">
						</div>
					</div>
			</div>
			<?php get_barraderecha();?>
		</div>
<?php get_footer(); ?>