<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carmenjorda');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'aildeg');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ktGN{7Nn>$xa+.?ml_-rq}Y|;{NA$spU,zn]R:OI&u`+Ja?3kDaq+Gm1awYWo>A(');
define('SECURE_AUTH_KEY',  'VcPcYT~L|iqc->su,7(~SJQH9U8z[TeFzi3RB_t#(,ZR4T_7|#Vm{AswJ!Tc>~W{');
define('LOGGED_IN_KEY',    '9qQ+L%h+$U}yhX!RTQD[:u.%4erC?4|vWRc)i;j:CH$xvG3sOV5|YmN|bMh 8OA+');
define('NONCE_KEY',        'M/>M@QA$`R-ijs%A&[<N*)->,N(OMZKDv+v^5]|+ugA,(xc}k{41IX/TmzC; /c#');
define('AUTH_SALT',        'xQSchc.J,&=ioqa1bu3+$RyC9l1cw6cLV _UAIY8VVcRl<_SL99x03h~Udoinefm');
define('SECURE_AUTH_SALT', '(|rY+;TvjmiEIqEwf@?h2knwpLXtxz)g07K8f`BBMPEIl|``psdm!-#aQC[&,8d$');
define('LOGGED_IN_SALT',   '*|)B[HY{qw%nnjtDG!J!B-WB)Zt#|Z&7BmSVZ2@aApP/+0vcC]83y0AFkZde{l8z');
define('NONCE_SALT',       '.o!zrrEUI]3Hj[ FCZ&:tF`WPJR[6O4*biyUs?^c$PIEY~e{m@iQgt6KgZ.O,]=h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
